const CopyWebpackPlugin = require('copy-webpack-plugin');
const extensionDir = 'extension/src/';
const safariExtensionDir = 'SecureBrowse.safariextension/src/';


module.exports = {
    entry: {
        'extension/src/background': './src/background.js',
        'SecureBrowse.safariextension/src/background': './src/safari_legacy_src/background.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname
    },
    plugins: [
        new CopyWebpackPlugin([
            // copy browser extension scripts
            {from: 'src/document_start.js', to: extensionDir + 'document_start.js'},
            {from: 'src/dashboard.js', to: extensionDir + 'dashboard.js'},
            {from: 'src/settings.js', to: extensionDir + 'settings.js'},
            {from: 'src/blocked.js', to: extensionDir + 'blocked.js'},
            // copy Safari legacy extension scripts
            {from: 'src/safari_legacy_src/document_start.js', to: safariExtensionDir + 'document_start.js'},
            {from: 'src/safari_legacy_src/document_end.js', to: safariExtensionDir + 'document_end.js'},
            {from: 'src/safari_legacy_src/dashboard.js', to: safariExtensionDir + 'dashboard.js'}
        ])
    ]
};
