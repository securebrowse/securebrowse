/**
 * This script is a tool for generating SecureBrowse integrity hash TXT records from a config file.
 * The securebrowse.config.js file should contain:
 *
 * module.exports = {
 *     txtRecords: [
 *         {
 *             urlPath: 'testsecurebrowse.org/a.css',
 *             filePaths: ['a.css']
 *         },
 *         {
 *             urlPath: 'testsecurebrowse.org/a.js',
 *             filePaths: ['a.js', '/another_script_for_a_b_testing/a.js']
 *         },
 *     ],
 *     maxTXTLength: 255,             // optional. Max is infinity if unset
 *     sriHashingAlgorithm: 'sha256', // optional. default is sha256
 *     source: '.',                   // optional. default is current directory
 *     destination: '.'               // optional. default is current directory
 * };
 *
 * Hashes are saved in securebrowse-hashes.txt. Every line should be a TXT record—including the
 * version
 *
 * TXT records are outputted in the form of:
 *      example.com/static/assets.js=<SHA hash> <SHA hash> <SHA hash> <SHA hash>...
 * Each object in the txtRecords array represents one TXT record. The urlPath shouldn't have a
 * http:// or www. prefix. If filePaths contains multiple file paths, then the hashes of those files
 * will be put into the same TXT record.
 *
 * If maxTXTLength is set, then integrity hash TXT records are split into TXT records that are under
 * that length. Those multiple TXT records will share the same urlPath.
 *
 * sriHashingAlgorithm is for the subresource integrity hash algorithm. Allowed algorithms are
 * sha256, sha384, sha512.
 *
 * source is the working directory for where the files are stored. All filePaths should be
 * respective to the source directory.
 *
 * destination is the directory for where the output securebrowse-hashes.txt will be created
 */

'use strict';
const fs = require('fs');
const crypto = require('crypto');
const config = require('./securebrowse.config.js');


// Validates input in config file
function validateInput() {
    if (!('txtRecords' in config)) {
        throw '"filePaths" in securebrowse.config.js not found';
    }
    if (!['number', 'undefined'].includes(typeof config.maxTXTLength) || config.maxTXTLength < 1) {
        throw '"maxTXTLength" in securebrowse.config.js must be a positive number';
    }
    if (!['sha256', 'sha384', 'sha512', undefined].includes(config.sriHashingAlgorithm)) {
        throw '"sriHashingAlgorithm" in securebrowse.config.js must be sha256, sha384, or sha512';
    }
    [config.source, config.destination].forEach(path => {
        if (path !== undefined &&
            !(fs.existsSync(path) && fs.lstatSync(path).isDirectory())) {
            throw path + ': No such directory';
        }
    });
    config.source = config.source ? config.source + '/' : '';
    config.destination = config.destination ? config.destination + '/' : '';
}

function readFile(filePath) {
    /**
     * Reads a file and returns a Promise of its contents
     * @param {string} filePath
     * @return {Promise} Contents of that file
     */
    return new Promise((resolve, reject) => {
        fs.readFile(config.source + filePath, 'utf8', (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
}

async function fetchFilePathContent(txtRecords) {
    /**
     * Fetches file contents indicated by filePaths indicated in txtRecords
     * @param {array} txtRecords Array of txtRecord objects. Each txtRecord contains the format:
     * {
     *      urlPath: 'testsecurebrowse.org/a.css',
     *      filePaths: ['a.css']
     * }
     * @return {array} A modified array of txtRecord objects. Their filePaths property is replaced
     * with a fileContents property. Each fileContents contains the entire content of their
     * respective files.
     */
    const whenTxtRecordsWithFileContents = txtRecords.map(
        txtRecord => new Promise((resolve, reject) => {
            const whenFileContents = txtRecord.filePaths.map(readFile);

            Promise.all(whenFileContents)
                .then(fileContents => {
                    resolve({
                        urlPath: txtRecord.urlPath,
                        fileContents
                    });
                })
                .catch(err => {
                    reject(err);
                });
        })
    );

    const txtRecordsWithFileContents = await Promise.all(whenTxtRecordsWithFileContents);
    return txtRecordsWithFileContents;
}

function generateHash(data) {
    /**
     * @param {string} data
     * @return {string} A base64-encoded hash of data
     */
    let algorithm = config.sriHashingAlgorithm || 'sha256';
    return algorithm + '-' + crypto.createHash(algorithm)
        .update(data)
        .digest('base64');
}

// Used to check a config file's format, and generate hashes from files in the config
function convertFileContentToHashes(txtRecords) {
    /**
     * Converts file contents in txtRecords to hashes
     * @param {array} txtRecords Array of txtRecord objects. Each txtRecord contains the format:
     * {
     *      urlPath: 'testsecurebrowse.org/a.css',
     *      fileContents: [<complete content of a.css>]
     * }
     * @return {array} A modified array of txtRecord objects. Their fileContents property is replaced
     * with a fileHashes property.
     */
    return txtRecords.map(txtRecord => {
        return {
            urlPath: txtRecord.urlPath,
            fileHashes: txtRecord.fileContents.map(generateHash).join(' ')
        };
    });
}

function exportTxtRecords(txtRecords) {
    /**
     * Exports file hashes to securebrowse-hashes.txt. If any hashes are larger than the
     * maxTXTLength character limit, they are split into multiple TXT records.
     * @param {array} txtRecords Array of txtRecord objects. Each txtRecord contains the format:
     * {
     *      urlPath: 'testsecurebrowse.org/a.css',
     *      fileHashes: ['sha256-3qLY6o924BINHEyK/hp9dv6famm378/7ltXz/Kz9orY=']
     * }
     */
    let data = 'SECUREBROWSE 1.0\n';
    const maxTXTLength = config.maxTXTLength;

    if (maxTXTLength) {
        txtRecords.forEach((f, i, arr) => {
            while (f.urlPath.length + f.fileHashes.length > maxTXTLength) {
                const divider = f.fileHashes
                    .substring(0, maxTXTLength - f.urlPath.length)
                    .lastIndexOf(' ');
                if (divider === -1) {
                    throw '"maxTXTLength" setting is too low';
                }
                const integrity = f.fileHashes.substring(0, divider);
                f.fileHashes = f.fileHashes.substring(divider + 1);
                arr.push({urlPath: f.urlPath, integrity});
            }
        });
    }

    data += txtRecords.map(f => f.urlPath + '=' + f.fileHashes).join('\n');
    fs.writeFile(config.destination + 'securebrowse-hashes.txt', data, err => {
        if (err) throw err;
        console.log(`TXT records are saved to ${config.destination}securebrowse-hashes.txt.`);
    });
}

async function main() {
    validateInput();
    fetchFilePathContent(config.txtRecords)
        .then(convertFileContentToHashes)
        .then(exportTxtRecords)
        .catch(error => {
            throw error;
        });
}

main();
