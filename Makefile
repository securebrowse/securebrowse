# Build dnssec modules for testing. You can edit /tests/tests.js and open /tests/test.html to see output.
build_dnssec_test_page:
	browserify tests/tests.js -o tests/bundle.js --debug
	echo Open and inspect /tests/test.html to see output

# Build and watch dnssec modules for testing. You can edit /tests/tests.js and open /tests/test.html to see output.
build_and_watch_dnssec_test_page:
	watchify tests/tests.js -o tests/bundle.js --debug
	echo Open and inspect /tests/test.html to see output

# Build browser extension. Load unpacked extensions from /extension/
build:
	sass src/scss/dashboard.scss extension/dashboard.css
	sass src/scss/settings.scss extension/settings.css
	sass src/scss/blocked.scss extension/blocked.css
	webpack-cli
	sass src/scss/safari-dashboard.scss SecureBrowse.safariextension/dashboard.css
	echo Build complete. Load \& install your extension in securebrowse-plugin/extension/

build_dev:
	sass src/scss/dashboard.scss extension/dashboard.css
	sass src/scss/settings.scss extension/settings.css
	sass src/scss/blocked.scss extension/blocked.css
	webpack-cli --devtool source-map
	sass src/scss/safari-dashboard.scss SecureBrowse.safariextension/dashboard.css
	echo Dev build complete. Load \& install your extension in securebrowse-plugin/extension/
