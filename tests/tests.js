/*eslint no-console: ["error", { allow: ["log", "error"] }] */
'use strict';
const dnssec = require('../src/dnssec');


let goodDnsTests = [
    'dnssec-name-and-shame.com',
    'example.com',
    'getdnsapi.net',
    'nlnetlabs.nl',
    'nlnet.nl',
    'verisigninc.com',
    'iis.se',
    'kirei.se',
    'ietf.org',
    'opendnssec.org',
    'labs.verisigninc.com',
    'blueprint.paypal.com',
    'eurid.eu',
    'ietf.org',
    'dns.pl',
    'danskebank.ie',
    'robustfiber.se',
    'ssnf.org',
    'ledningskollen.se',
    'ekazakov.ru',
    'search.usa.gov',
    'uninett.no',
    'not-pasteb.in',
    'regeringen.se',
    'danskebank.se',
    'alandsbanken.se',
    'handelsbanken.se',
    'ip4afrika.nl',
    'dnsbelgium.be',
    'apnic.net',
    'internet.nl'
];

let badDnsTests = [
    'google.com',
    'facebook.com',
    'youtube.com',
    'yahoo.com',
    'baidu.com',
    'wikipedia.org',
    'qq.com',
    'twitter.com',
    'live.com',
    'amazon.com',
    'taobao.com',
    'linkedin.com',
    'google.co.in',
    'sina.com.cn',
    'blogspot.com',
    'hao123.com',
    'weibo.com',
    'yahoo.co.jp',
    'wordpress.com',
    'vk.com',
    'yandex.ru',
    'ebay.com',
    'bing.com',
    'tmall.com',
    'google.de'
];

async function runFullTests() {
    console.log('Testing on DNSSEC-verified domains:\n');
    let failedTests = 0;
    for (var i = 0; i < goodDnsTests.length; i++) {
        try {
            await dnssec.validateDnssec(goodDnsTests[i],['A', 'AAAA', 'TXT']);
        } catch (e) {
            failedTests++;
        }
    }
    if (failedTests !== 0) {
        alert('Tests that should pass are failing!');
    }

    goodDnsTests.forEach(dnssec.validateDnssec);
    console.log('\nTesting on non-DNSSEC-verified domains:\n');
    failedTests = 0;
    for (var j = 0; j < badDnsTests.length; j++) {
        try {
            await dnssec.validateDnssec(badDnsTests[j],['A', 'AAAA', 'TXT']);
        } catch (e) {
            failedTests++;
        }
    }

    if (failedTests !== j) {
        alert('Tests that should be failed are passing!');
    }
}

async function runExampleComTest() {
    console.log('Testing on example.com for DNSSEC verified A, AAAA, and TXT records:');
    console.log('(-1=INSECURE, 0=NOT_FOUND, 1=SECURE)');
    let exampleComResults = await dnssec.validateDnssec('example.com', ['A', 'AAAA', 'TXT']);
    console.log(exampleComResults);
    for (let record in exampleComResults) {
        if (exampleComResults[record] === -1){
            console.error(record + ' RECORD IS INSECURE');
        }
    }
}

runExampleComTest();
// runFullTests();
