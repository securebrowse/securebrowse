# SecureBrowse Browser Extension

![icon](/extension/images/Icon-128.png)

A Google Chrome, Mozilla Firefox, Microsoft Edge, and Safari extension used to verify web page resource file content via Subresource Integrity and DNSSEC Validation.

## Overview

One of the most critical issues with securing web applications is that the client-side code is delivered every time one opens the page. This means that a compromised web server or network component could change the client-side code and exfiltrate supposedly secure client-side data.

SecureBrowse is a security protocol designed to protect against that. Using SecureBrowse, clients can validate the integrity of highly sensitive website resources and block subsequent client-side execution of compromised code. It does this with the use of a browser extension and DNSSEC-signed TXT records.

First, a provider must take their resources (client-side html, css, & js files) and generate hashes of their content (`sha256`, `sha384`, `sha512` only). Those hashes must then be stored on DNSSEC-signed TXT records on the domain of what they want to secure.

When a client visits that domain, a SecureBrowse browser extension will generate the hash digest of every resource content of that domain, and check for a match with the corresponding hashes stored on those TXT records. If no match is found, that resource is blocked. If a match is found, then the resource is allowed. This can prevent the client from ever running compromised scripts or visiting HTML pages that are compromised.

[The SecureBrowse Wiki](https://gitlab.com/securebrowse/securebrowse/wikis/Home)

[The SecureBrowse RFC](https://gitlab.com/securebrowse/securebrowse/wikis/The-SecureBrowse-RFC)

### Contributors

- 🐴 Security scheme designed by [Riccardo Spagni](https://twitter.com/fluffypony)
- 🍵 Built and maintained by [Brian Chuk](http://www.brianch.uk/)
- 🎒 Maintained by [Paul Shapiro](http://github.com/paulshapiro)
- 🦅 Security audit, testing, & RFC authoring by [Zack Fasel](https://twitter.com/zfasel?lang=en) and [Urbane Security](https://urbanesecurity.com/)

### Get in touch

Freenode IRC: #securebrowse

### Acknowledgements

- Hosted by [MyMonero](https://mymonero.com/)
- Icons from [Feather Icons](https://feathericons.com/)
