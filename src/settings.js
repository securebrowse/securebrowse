/**
 * This script manipulates the extension settings popup html UI.
 * Upon navigating to the settings page, this script is run and sends a request to background.js to get
 * the user's settings data.
 */
'use strict';
window.browser = window.msBrowser || window.browser || window.chrome;


class DomainList {
    constructor(ulId) {
        this.ul = document.getElementById(ulId);
        this.domains = [];
    }

    li(domain) {
        return `<li>${domain} <img class="delete" src="./images/trash.svg"/></li>`;
    }

    addDomain(domain) {
        this.ul.insertAdjacentHTML('afterbegin', this.li(domain));
        const dl = this;
        this.ul.children[0].lastElementChild.addEventListener('click', () => {
            dl.removeDomain(domain);
        }, false);
        this.domains.push(domain);
    }

    removeDomain(domain) {
        const li = Array.from(this.ul.children).find(childLi => childLi.innerText === domain);
        this.ul.removeChild(li);
        this.domains.splice(this.domains.indexOf(domain), 1);
        // if confirmed, send a msg to cache remove
        if (this.ul.id === 'domains') {
            browser.runtime.sendMessage({type: 'removeDomain', domain});
        } else {
            browser.runtime.sendMessage({type: 'removeInsecureDomain', domain});
        }
    }

    includes(domain) {
        return this.domains.includes(domain);
    }
}

function extractHostname(url) {
    let hostname;
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }
    hostname = hostname.split(':')[0];
    hostname = hostname.split('?')[0];
    return hostname;
}

function extractRootDomain(url) {
    let domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;
    // extracting the root domain here
    // if there is a subdomain
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        // check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            // this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}

let pendingDomains = new DomainList('pending-domains');
let confirmedDomains = new DomainList('domains');

function updateSettings(data) {
    data.secureDomains.forEach(domain => confirmedDomains.addDomain(domain));
    data.insecureDomains.forEach(domain => pendingDomains.addDomain(domain));
}

document.getElementById('submit').addEventListener('click', () => {
    const domain = extractRootDomain(
        document.getElementsByTagName('textarea')[0].value
            .replace(/\s/g,'')
    );
    if (domain === '') {
        return;
    }
    document.getElementsByTagName('textarea')[0].value = '';
    if (pendingDomains.includes(domain)) {
        return;
    }
    pendingDomains.addDomain(domain);
    browser.runtime.sendMessage({
        type: 'verify',
        domain
    });
});


browser.runtime.sendMessage({type: 'getCachedDomains'}, updateSettings);

browser.runtime.onMessage.addListener((request) => {
    if (request.type === 'settings') {
        if (request.secure) {
            pendingDomains.removeDomain(request.domain);
            confirmedDomains.addDomain(request.domain);
        } else {
            console.error(request.error);
            if (request.error.name === 'OutdatedExtensionException') {
                alert('A new SecureBrowse security protocol version has been detected. Have you updated your plugin? SecureBrowse will continue to attempt to reverify it.');
                return;
            } else if (request.error.name === 'ValidationFailureException') {
                alert('There was an issue when attempting to verify the domain. SecureBrowse will continue to attempt to reverify it. \n' + request.error.message);
                return;
            }
            alert('There was an issue when attempting to verify the domain. It may be insecure.\nSecureBrowse will continue to attempt to reverify it.');
        }
    }
});
