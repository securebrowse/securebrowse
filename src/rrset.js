/**
 * Container for a group of RDatas
 */
'use strict';


// Container of a group of RDatas
class RRset {
    /**
     * Container of a group of RDatas
     * @property {Name} name The domain name the RRset is located in
     * @property {string} rdtype Contains the resource records type. Example: 'RRSIG'
     * @property {array} list Array of rdatas
     */
    constructor(name, rdtype, rdatas) {
        this.name = name;
        this.rdtype = rdtype;
        this.list = rdatas;
    }
}

exports.RRset = RRset;
