/**
 * The background-utils script contains some useful functions for setting up integrity field checks.
 */
'use strict';
const crypto = require('crypto');

function cutStringAtChar(s, searchChar) {
    /**
     * Splits a string at the index of the first occurrence of the specified search character
     * @param s
     * @param searchChar A string representing the character to search for
     * @return {array} [<substring before the searchChar>, <substring after the searchChar>]
     */
    const divider = s.indexOf(searchChar);
    return [s.substring(0, divider), s.substring(divider + 1)];
}

function buildSRIHashRecordFromTxtRRSet(txtRRSet) {
    /**
     * Converts an RRset of TXT records into an easy-to-read object of key-value pairs
     * The TXT records are expected to contain a keyvalue pair for one file's SRI info
     * Example TXT record: IN TXT "test.com/filename.js=sha256-Mo/XcsJT6rMNNywuzTFL0akmlGLfpbC5QqEKsrihJbw="
     * The resulting key-value pair would become:
     *     test.com/filename.js -- sha256-Mo/XcsJT6rMNNywuzTFL0akmlGLfpbC5QqEKsrihJbw=
     * If multiple TXT records have the same urlPath, squash them into one TXT record. This is
     * useful for AB testing
     * @param {RRset} txtRRset The rrset to reformat
     * @return {object} object of urlPath-integrity pairs
     */
    const rrlist = txtRRSet.TXT.list.filter((txt) => {
        // Filter out any TXT records that don't contain SRI hashes
        return ['sha256-', 'sha384-', 'sha512-'].some(substr => txt.getString().includes(substr));
    });
    let sriHashRecords = {};
    for (let i = 0; i < rrlist.length; i++) {
        const [urlPath, integrity] = cutStringAtChar(rrlist[i].getString(), '=');
        if (urlPath in sriHashRecords) {
            sriHashRecords[urlPath] += ' ' + integrity;
        } else {
            sriHashRecords[urlPath] = integrity;
        }
    }
    if (rrlist) {
        sriHashRecords.expiration = rrlist[0].expiration;
        sriHashRecords.whitelist = getSecureBrowseWhitelist(txtRRSet)
    }
    return sriHashRecords;
}

function requestFileContentSync(url) {
    /**
     * Retrieves resource content hosted on the url synchronously
     * @param url
     * @return {string} content of the resource
     */
    const request = new XMLHttpRequest();
    request.open('GET', url, false);
    request.send(null);
    return request.responseText;
}

async function requestFileContentAsync(url, type = undefined) {
    /**
     * Retrieves resource content hosted on the url asynchronously
     * @param url
     * @return {Promise<string>} content of the resource
     */
    try {
        const content = await fetch(url).then(response => response.text());
        return content;
    } catch (err) {
        err.type = type;
        throw err;
    }
}

function hash(content, algorithm) {
    /**
     * Generate a hash digest via the given hash algorithm from the content data given
     * @param {string} content content of the resource
     * @param {algorithm} algorithm Options: sha256, sha384, sha512
     * @return {string} the hash digest in base64 format
     */
    return crypto.createHash(algorithm).update(content).digest('base64');
}

function checkForMatch(hashes, content) {
    /**
     * Checks if a hashDigest of the content matches any of the hashes given
     * @param {Array} hashes In the form of sha256-<digest>
     * @param {string} content content of the resource
     * @return {boolean}
     */
    return hashes.some(integrity => {
        const [algorithm, digest] = cutStringAtChar(integrity, '-');
        return digest === hash(content, algorithm);
    });
}

function getSecureBrowseVersion(txtRRSet) {
    /**
     * Searches for a SecureBrowse version from an rrset of TXT records
     * @param {RRset} txtRRSet
     * @return {string} version number
     */
    const versionTXTRecord = txtRRSet.TXT.list.find(txt => txt.getString().startsWith('SECUREBROWSE '));
    if (versionTXTRecord === undefined) {
        return 'NOT_FOUND';
    } else {
        return versionTXTRecord.getString().substr(13);
    }
}

function getSecureBrowseWhitelist(txtRRSet) {
    /**
     * Searches for a SecureBrowse whitelist from an rrset of TXT records
     * @param {RRset} txtRRSet
     * @return {Array} whitelisted urls
     */
    const versionTXTRecord = txtRRSet.TXT.list.find(txt => txt.getString().startsWith('SECUREBROWSE-WHITELIST '));
    if (versionTXTRecord === undefined) {
        return [];
    } else {
        return versionTXTRecord.getString().substr(23).split(' ');
    }
}

module.exports = {
    buildSRIHashRecordFromTxtRRSet,
    getSecureBrowseVersion,
    checkForMatch,
    requestFileContentAsync,
    requestFileContentSync
};
