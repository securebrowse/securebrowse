/**
 * Resource records are represented by separate children classes of RData.
 */
'use strict';
const jsPack = require('jspack').jspack;
const Name = require('./name').Name;
const NotImplementedException = require('./exceptions').NotImplementedException;


class RData {
    /**
     * Base class for all DNS rdata types
     * @property {number} rdtype Contains the resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     */
    constructor(type, ttl) {
        this.rdtype = type;
        this.expiration = Date.now() + ttl * 1000;
    }

    static fromString(type, ttl, data) {
        return new this(type, ttl, ...data.split(' '));
    }

    toString() {
        throw new NotImplementedException('');
    }

    // Convert data into wire format. Implemented in child classes
    toWire() {
        throw new NotImplementedException('');
    }
}

class A extends RData {
    /**
     * A record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {string} address An IPv4 address in standard 'dotted quad' format
     */
    constructor(type, ttl, address) {
        super(type, ttl);
        // TODO: check that it's a valid address
        this.address = address;
    }

    toString() {
        return this.rdtype + ' ' + this.address;
    }

    toWire() {
        let parts = this.address.split('.');
        if (parts.length != 4) {
            throw new SyntaxError();
        }
        for (let i = 0; i < parts.length; i++) {
            if (!Number.isInteger(Number(parts[i]))) {
                throw new SyntaxError();
            }
            if (parts[i].length > 1 && parts[i][0] === '0') {
                // No leading zeros
                throw new SyntaxError();
            }
        }
        return String.fromCodePoint(...jsPack.Pack('BBBB', parts.map(Number)));
    }
}

class AAAA extends RData {
    /**
     * AAAA record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {string} address An IPv6 address
     */
    constructor(type, ttl, address) {
        super(type, ttl);
        // TODO: check that it's a valid address
        this.address = address;
    }

    toString() {
        return this.rdtype + ' ' + this.address;
    }

    toWire() {
        let text = this.address;
        if (text === '::') {
            text = '0::';
        }
        // TODO: check for dot quad syntax
        if (text.slice(0,2) === '::') {
            text = text.slice(1);
        } else if ((text.slice(-2) === '::')) {
            text = text.slice(0,-1);
        }
        //Now canonicalize into 8 chunks of 4 hex digits each
        let chunks = text.split(':');
        const l = chunks.length;
        if (l > 8) {
            throw new SyntaxError();
        }
        let encounteredEmpty = false;
        let canonical = [];
        for (let i = 0; i < chunks.length; i++) {
            if (chunks[i] === '') {
                if (encounteredEmpty) {
                    throw new SyntaxError();
                }
                encounteredEmpty = true;
                for (let j = 0; j < 8 - l + 1; j++) {
                    canonical.push('0000');
                }
            } else {
                let lc = chunks[i].length;
                if (lc > 4) {
                    throw new SyntaxError();
                }
                if (lc !== 4) {
                    chunks[i] = Array(4 - lc + 1).join('0') + chunks[i];
                }
                canonical.push(chunks[i]);
            }
        }
        if (l < 8 && !encounteredEmpty) {
            throw new SyntaxError();
        }
        text = canonical.join('');
        return new Buffer(text, 'hex').toString('binary');
    }
}

class DNSKEY extends RData {
    /**
     * DNSKEY record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {number} flags The key flags (256 or 257)
     * @property {number} protocol The protocol for which key may be used
     * @property {number} algorithm The algorithm used for the key
     * @property {string} key The public key, in base64 format
     */
    constructor(type, ttl, flags, protocol, algorithm, key) {
        super(type, ttl);
        this.flags = Number(flags);
        this.protocol = Number(protocol);
        this.algorithm = Number(algorithm);
        this.key = key;
    }

    toString() {
        return this.rdtype + ' ' +
        this.flags + ' ' +
        this.protocol + ' ' +
        this.algorithm + ' ' +
        this.key;
    }

    toWire() {
        let header = jsPack.Pack('!HBB', [this.flags, this.protocol, this.algorithm]);
        return String.fromCodePoint(...header) + window.atob(this.key);
    }
}

class RRSIG extends RData {
    /**
     * RRSIG record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {number} rdtypeCovered The rdata type this signature
     * @property {number} expiration RR expiration in Unix time in ms covers
     * @property {number} algorithm The algorithm used for the signature
     * @property {number} labels Number of labels
     * @property {number} originalTTL The original TimeToLive
     * @property {number} expiration Signature expiration time
     * @property {number} inception Signature inception time
     * @property {number} keyTag The key tag
     * @property {Name} signer The signer
     * @property {number} signature The signature, in base64 format
     */
    constructor(type, ttl, typeCovered, algorithm, labels, originalTTL,
        expiration, inception, keyTag, signer, signature) {
        super(type, ttl);
        this.typeCovered = typeCovered.toUpperCase();
        this.algorithm = Number(algorithm);
        this.labels = Number(labels);
        this.originalTTL = Number(originalTTL);
        this.expiration = convertToUnixTime(expiration);
        this.inception = convertToUnixTime(inception);
        this.keyTag = Number(keyTag);
        this.signer = Name.fromString(signer);
        this.signature = signature;
    }

    toString() {
        return this.rdtype + ' ' +
        this.typeCovered + ' ' +
        this.algorithm + ' ' +
        this.labels + ' ' +
        this.originalTTL + ' ' +
        this.expiration + ' ' +
        this.inception + ' ' +
        this.keyTag + ' ' +
        this.signer + ' ' +
        this.signature;
    }

    toWire() {
        // TODO: Does not return signer.toWire data. Append if needed.
        const typeCovered = byText[this.typeCovered.toUpperCase()];
        let header = jsPack.Pack('!HBBIIIH', [typeCovered, this.algorithm, this.labels,
            this.originalTTL, this.expiration, this.inception, this.keyTag]);
        return String.fromCodePoint(...header) + window.atob(this.signature);
    }
}

class DS extends RData {
    /**
     * DS record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {number} keyTag The key tag
     * @property {number} algorithm The algorithm used for the signature
     * @property {number} digestType The digest type
     * @property {string} digest The digest
     */
    constructor(type, ttl, keyTag, algorithm, digestType, digest) {
        super(type, ttl);
        this.keyTag = Number(keyTag);
        this.algorithm = Number(algorithm);
        this.digestType = Number(digestType);
        this.digest = digest;
    }

    toString() {
        return this.rdtype + ' ' +
        this.keyTag + ' ' +
        this.algorithm + ' ' +
        this.digestType + ' ' +
        this.digest;
    }

    toWire() {
        let header = jsPack.Pack('!HBB', [this.keyTag, this.algorithm, this.digestType]);
        const binaryDigest = new Buffer(this.digest, 'hex').toString('binary');
        return String.fromCodePoint(...header) + binaryDigest;
    }
}

class TXT extends RData {
    /**
     * TXT record
     * @property {number} rdtype Resource record type
     * @property {number} expiration RR expiration in Unix time in ms
     * @property {array<string>} strings The misc strings stored within the record.
     */
    constructor(type, ttl, strings) {
        super(type, ttl);
        this.strings = strings;
    }

    toString() {
        let text = '';
        let prefix = '';
        for (let i = 0; i < this.strings.length; i++) {
            text += `${prefix}"${this.strings[i]}"`;
            prefix = ' ';
        }
        return text;
    }

    getString() {
        return this.strings.join('');
    }

    static fromString(type, ttl, data) {
        let quotes = new RegExp('"', 'g');
        // TXT record strings can only have 255 characters. Anything more, and they are
        // separated as multiple strings: "<string1>" "<string2>"
        data = data
            .split('" "')
            .map(s => s.replace(quotes, ''));
        return new this(type, ttl, data);
    }

    toWire() {
        let data = '';
        this.strings.forEach(s => {
            const l = s.length;
            if (l > 255) {
                throw new SyntaxError();
            }
            data += String.fromCodePoint(...jsPack.Pack('!B', [l]));
            data += s;
        });
        return data;
    }
}

var rdataClasses = {
    A,
    AAAA,
    DNSKEY,
    RRSIG,
    DS,
    TXT
};

var byText = {
    A: 1,
    AAAA: 28,
    DNSKEY: 48,
    RRSIG: 46,
    DS: 43,
    TXT: 16
};

// Flip of byText
var byValue = {};
for (var key in byText) {
    byValue[byText[key]] = key;
}

function getRdClassFromType(type) {
    /**
     * Returns a class based on the rdtype input
     * @param {number} type The rdata type
     * @return {class} The corresponding RData class. undefined if it's not implemented
     */
    if (!(type in byValue)) {
        return undefined;
    }
    let rdClass = rdataClasses[byValue[type]];
    return rdClass;
}

function getRDataFromString(type, ttl, str) {
    /**
     * Wrapper for static RData.fromString function to check for classes not implemented yet
     * @param {number} type The resource record data type
     * @param {string} str The resource record data
     * @return {obj} A new object of the corresponding RData class.
     * undefined if it's not implemented
     */
    let cls = getRdClassFromType(type);
    return cls === undefined ? undefined : cls.fromString(type, ttl, str);
}

function rdataCompare(a, b) {
    // Compares an Rdata with another Rdata of the same rdtype
    const aWire = a.toWire();
    const bWire = b.toWire();
    if (a === b) {
        return 0;
    } else if (aWire > bWire) {
        return 1;
    } else {
        return -1;
    }
}

function convertToUnixTime(s) {
    /**
     * @param {number || string} s a date. Example format: 20180712143007
     * @return {number} a UNIX timestamp. Example format: 1530250441
     */
    s += '';
    let date = new Date(
        Date.UTC(
            s.substring(0,4),   // year
            s.substring(4,6)-1, // month, zero-indexed
            s.substring(6,8),   // day
            s.substring(8,10),  // hour
            s.substring(10,12), // minute
            s.substring(12,14)  // second
        )
    );
    return Date.parse(date) / 1000;
}

module.exports = {
    byText,
    byValue,
    getRdClassFromType,
    getRDataFromString,
    RData,
    rdataClasses,
    rdataCompare
};
