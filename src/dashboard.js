/**
 * This script manipulates the extension dashboard popup html UI with information about the page's
 * security status.
 * Upon opening the popup, this script sends a request to background.js to get security data about
 * the web page the user is on.
 */
'use strict';
window.browser = window.msBrowser || window.browser || window.chrome;


const check = './images/GoodIcon.svg';
const x = './images/BadIcon.svg';

function el(id) {
    return document.getElementById(id);
}

// Create a list detail item and append to <ul> element
function addListItem(status, type) {
    if ((type !== 'html' && status.secure + status.insecure === 0) ||
        status === undefined) {
        return;
    }
    let secure = false;
    let text = '';
    if (type === 'html') {
        secure = status;
        text = `HTML ${secure ? 'Verified' : 'Insecure'}`;
    } else {
        secure = status.insecure === 0;
        const amount = secure ? status.secure : status.insecure;
        const state = secure ? 'Verified' : 'Insecure';
        if (amount === 1) {
            type = type.slice(0, -1);
        }
        text = `${amount} ${type} ${state}`;
    }
    el('info').innerHTML += li(
        secure ? 'shield-safe' : 'shield-unsafe',
        text
    );
}

// Return template string of a <li> element with class and text
function li(cls, txt) {
    return `<li>
        <span class="${cls}"></span>
        <span>${txt}</span>
    </li>`;
}

function isSecure(tabStatus) {
    return !tabStatus.script.insecure && !tabStatus.stylesheet.insecure && tabStatus.htmlSecure &&
        !tabStatus.everythingIsInsecure && !tabStatus.phishingDetected && !tabStatus.outdated;
}

function updateSecurityBadge(secure) {
    if (secure) {
        el('hero').firstChild.src = check;
        el('status').innerHTML = 'WEB PAGE VERIFIED';
    } else {
        el('hero').firstChild.src = x;
        el('status').innerHTML = 'WEB PAGE INSECURE';
    }
}

function updateDashboard(tabStatus) {
    el('explanation').style.display = 'None';
    el('url').innerHTML = tabStatus.url;
    if (tabStatus.outdated) {
        el('explanation').innerHTML = 'A new SecureBrowse security protocol version has been detected. Have you updated your plugin?';
        el('explanation').style.display = '';
        updateSecurityBadge(false);
        return;
    }
    if (!tabStatus.sbEnabled) {
        el('status').innerHTML = 'Verification Not Enabled';
        return;
    }

    if (isSecure(tabStatus)) {
        updateSecurityBadge(true);
    } else {
        updateSecurityBadge(false);

        let dialog = 'There are insecure resources found. Attackers might be trying to steal your information.';
        if (tabStatus.blockedInsecureResources) {
            dialog = 'There are insecure resources found and blocked. Attackers might be trying to steal your information.';
        }
        if (tabStatus.phishingDetected) {
            dialog = `Looks like you're on the wrong website.
            Make sure that the URL is actually the correct one.`;
        }
        el('explanation').innerHTML = dialog;
        el('explanation').style.display = '';

        if (tabStatus.everythingIsInsecure) {
            el('info').innerHTML += li('shield-unsafe', 'All resources are insecure');
            return;
        }
    }
    addListItem(tabStatus.script, 'Scripts');
    addListItem(tabStatus.stylesheet, 'Stylesheets');
    addListItem(tabStatus.htmlSecure, 'html');

    // el('info').innerHTML += li(
    //     'phishing-' + (tabStatus.phishingDetected ? 'unsafe' : 'safe'),
    //     (tabStatus.phishingDetected ? '' : 'No ') + 'Mispelling/Phishing Detected'
    // );
}

function openSettings() {
    const settingsUrl = browser.extension.getURL('settings.html');
    browser.tabs.query({url: settingsUrl}, (tabs) => {
        if (tabs.length > 0) {
            browser.tabs.update(tabs[0].id, {active: true});
        } else {
            browser.tabs.create({url: settingsUrl, active: true}, (tab) => {
                browser.windows.update(tab.windowId, {focused: true});
            });
        }
        window.close();
    });
}

el('settings').onclick = openSettings;

setTimeout(() => {
    // Send a request to background.js for security data about the web page the user is on
    browser.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        browser.runtime.sendMessage({
            type: 'tabInfo',
            tabID: tabs[0].id,
            tabURL: tabs[0].url
        }, (response) => {
            updateDashboard(response);
            let style = el('dashboard').style;
            style.display = 'block';
            setTimeout(() => {
                style.opacity = 1;
            });
        });
    });
}, 300);
