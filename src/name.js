/**
 * The name module handles functions and classes related to domain names.
 */

'use strict';
const jsPack = require('jspack').jspack;


function removeQueryAndHash(url) {
    url = url.split('?')[0];
    url = url.split('#')[0];
    return url;
}

function extractHostname(url) {
    let hostname;
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }
    hostname = hostname.split(':')[0];
    hostname = hostname.split('?')[0];
    hostname = hostname.split('#')[0];
    return removePrefix(hostname);
}

function extractRootDomain(url) {
    let domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;
    // extracting the root domain here
    // if there is a subdomain
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        // check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            // this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}

function extractPathName(url) {
    let l = document.createElement('a');
    l.href = url;
    // The substring method removes the prefix '/' in the path
    return l.pathname.substring(1);
}

function removePrefix(url) {
    return url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, '');
}

class Name {
    /**
     * Represents a DNS name as an array of labels
     * @property {Array} labels
     */
    constructor(labels) {
        this.labels = labels;
    }

    // Converts a URL string into a new Name object
    static fromString(data) {
        const hostname = extractHostname(data);
        if (hostname === '.') {
            return new this(['']);
        } else {
            return new this(hostname.split('.'));
        }
    }

    toString() {
        if (this.labels.length === 1 && this.labels[0] === '') {
            return '.';
        }
        return this.labels.join('.');
    }

    // Convert name into wire format
    toWire() {
        let labels = this.labels;
        let wire = '';
        for (let i = 0; i < labels.length; i++) {
            let labelLength = labels[i].length;
            wire += String.fromCodePoint(...jsPack.Pack('!B', [labelLength]));
            if (labelLength > 0) {
                wire += labels[i];
            }
        }
        return wire;
    }


    getAllNames(rootLabel = false) {
        /**
         * Converts a name into a list of FQDNs
         * @param {bool} rootLabel Add trailing root zone (.) to domain names
         * @return {array} a list of FQDNs
         */
        let names = [];
        for (let i = 0; i < this.labels.length; i++) {
            let domainName = this.labels.slice(i).join('.');
            if (rootLabel) { // example: google.com --> google.com.
                domainName += '.';
            }
            names.push(domainName);
        }
        names.push('.');
        return names;
    }

    split(depth) {
        /**
         * Split a DNS name into prefix and suffix names based on specific depth
         * @param {number} depth An int specifying number of labels in the suffix
         * @return {array} [prefix Name, suffix Name]
         */
        const l = this.labels.length;
        if (depth === 0) {
            return [this, new this.constructor([])];
        } else if (depth === l) {
            return [new this.constructor([]), this];
        } else if (depth < 0 || depth > l) {
            throw new RangeError('depth must be >= 0 and <= the length of the name');
        } else {
            return [new this.constructor(this.labels.slice(0, l - depth)),
                new this.constructor(this.labels.slice(l - depth))];
        }

    }
}

module.exports = {
    extractHostname,
    extractRootDomain,
    extractPathName,
    removePrefix,
    removeQueryAndHash,
    Name
};
