/**
 * The alerts script helps keep track of the security status of the tabs a user visits. It's also
 * responsible for notifying the user about safe and unsafe resources they come across
 */
'use strict';
const goodIcon = './images/GoodIcon-48.png';
const badIcon = './images/BadIcon-48.png';
window.browser = window.msBrowser || window.browser || window.chrome;

class TabStatus {
    /**
     * Security data about a tab
     * @property {boolean} sbEnabled Indicates whether SecureBrowse verification is enabled for tab
     * @property {string} url Root domain url of the tab
     * @property {boolean} blockedInsecureResources Indicates whether insecure resources are blocked
     * @property {boolean} everythingIsInsecure Indicates whether all resources are not secured
     * @property {boolean} outdated Indicates whether the extension version is lower than the SB Protocol
     * @property {boolean} phishingDetected Indicates a potential phishing attack
     * @property {object} script Records number of secure and insecure script resources
     * @property {object} stylesheet Records number of secure and insecure CSS resources
     * @property {boolean} htmlSecure Indicates whether the HTML content of initial page is secure
     */
    constructor(url) {
        this.sbEnabled = true;
        this.url = url;
        this.blockedInsecureResources = false;
        this.everythingIsInsecure = false;
        this.phishingDetected = false;
        this.outdated = false;
        this.script = {
            secure: 0,
            insecure: 0
        };
        this.stylesheet = {
            secure: 0,
            insecure: 0
        };
        this.htmlSecure = undefined;
    }

    isSecure() {
        return !this.script.insecure && !this.stylesheet.insecure &&
        this.htmlSecure && !this.everythingIsInsecure && !this.outdated;
    }
}

class Alerts {
    /**
     *
     * @property {object} tabStatuses An object of key-value pairs. The keys are tabIds, values are
     * TabStatus objects
     */
    constructor(tabStatuses) {
        this.tabStatuses = tabStatuses;
    }

    sendAlert(tabId, message) {
        /**
         * Sends a message string to the tab with tabId. The tab then uses alert() to relay the msg
         * @param {Number} tabId the id of the tab to receive the msg
         * @param {string} message the message to send
         */
        browser.tabs.sendMessage(tabId, {type: 'alert', message});
    }

    reportPhishingDetected(tabId, hostname) {
        /**
         * Notifies the user that the extension has detected a potential phishing attack.
         * @param {Number} tabId
         * @param {string} hostname the root domain of the tab
         */
        this.tabStatuses[tabId] = new TabStatus(hostname);
        this.tabStatuses[tabId].phishingDetected = true;
        this.sendAlert(
            tabId,
            'SecureBrowse: Potential URL mispelling/phishing detected. Please check the URL.'
        );
        this.setIcon(tabId);
    }

    reportConnectionError(tabId, hostname) {
        /**
         * Notifies the user that the extension has had an issue connecting to the Cloudflare
         * DNS-over-HTTPS api.
         * @param {Number} tabId
         * @param {string} hostname the root domain of the tab
         */
        this.tabStatuses[tabId] = new TabStatus(hostname);
        this.tabStatuses[tabId].everythingIsInsecure = true;
        this.sendAlert(
            tabId,
            'Error: SecureBrowse is unable to connect to the DNS-over-HTTPS server.'
        );
        this.setIcon(tabId);
    }

    reportExtensionNeedsUpdate(tabId, hostname) {
        /**
         * Notifies the user that the extension has detected a new SecureBrowse Security Protocol
         * Version for a tab.
         * @param {Number} tabId
         * @param {string} hostname the root domain of the tab
         */
        this.tabStatuses[tabId] = new TabStatus(hostname);
        this.tabStatuses[tabId].outdated = true;
        this.sendAlert(
            tabId,
            'The SecureBrowse extension may be outdated. Please update your extension.'
        );
        this.setIcon(tabId);
    }

    reportSafeResource(tabId, hostname, type, create = true) {
        /**
         * Tracks one safe resource found. Its information is added to the corresponding TabStatus
         * @param {Number} tabId the id of the tab the resource is found on
         * @param {string} hostname Root domain url of the tab
         * @param {string} type Options: script, stylesheet, main_frame
         */
        if (type === 'main_frame') {
            if (create) {
                this.tabStatuses[tabId] = new TabStatus(hostname);
            }
            const tabStatus = this.tabStatuses[tabId];
            tabStatus.htmlSecure = true;
        } else {
            const tabStatus = this.tabStatuses[tabId];
            tabStatus[type].secure++;
        }
    }

    reportUnsafeResource(tabId, hostname, type, blocked = true, create = true) {
        /**
         * Tracks one unsafe resource found. Its information is added to the corresponding TabStatus
         * A user is also notified of the resource
         * @param {Number} tabId the id of the tab the resource is found on
         * @param {string} hostname Root domain url of the tab
         * @param {string} type Options: script, stylesheet, main_frame
         * @param {boolean} blocked Indicates whether the resource was blocked
         */
        switch (type) {
        case 'all': {
            if (create) {
                this.tabStatuses[tabId] = new TabStatus(hostname);
            }
            const tabStatus = this.tabStatuses[tabId];
            tabStatus.everythingIsInsecure = true;
            this.sendAlert(tabId, 'Everything on this web page is unverified due to unsigned DNSSEC. Please check SecureBrowse.');
            break;
        }
        case 'main_frame': {
            this.tabStatuses[tabId] = new TabStatus(hostname);
            const tabStatus = this.tabStatuses[tabId];
            tabStatus.blockedInsecureResources = blocked;
            tabStatus.htmlSecure = false;
            this.sendAlert(tabId, 'The HTML on this web page is insecure. Please check SecureBrowse.');
            break;
        }
        default: {
            const tabStatus = this.tabStatuses[tabId];
            tabStatus[type].insecure++;
            tabStatus.blockedInsecureResources = blocked;
            if (tabStatus.script.insecure + tabStatus.stylesheet.insecure === 1) {
                let message = 'Insecure resources have been detected. Please check SecureBrowse.';
                if (tabStatus.blockedInsecureResources) {
                    message = 'Insecure resources have been detected and blocked. Please check SecureBrowse.';
                }
                this.sendAlert(tabId, message);
            }
        }
        }
        this.setIcon(tabId);
    }

    setIcon(tabId) {
        /**
         * Updates the extension toolbar icon for a tab based on its TabStatus
         * @param {Number} tabId The id of the tab selected. The icon will only be displayed for
         * that tab
         */
        const tabStatus = this.tabStatuses[tabId];
        if (tabStatus) {
            let icon = badIcon;
            if (tabStatus.isSecure()) {
                icon = goodIcon;
            }

            browser.browserAction.setIcon({
                path: icon,
                tabId: tabId
            });
        }
    }

    disableSbTab(tabId, url) {
        if (url.startsWith(browser.extension.getURL('blocked.html'))) {
            return;
        }
        delete this.tabStatuses[tabId];
    }
}

module.exports = Alerts;
