/**
 * The Cache class stores urlPath--hashDigest pairs under domains. The cache is stored as a JSON obj
 * in the following format:
 *
 * {
 *     domains: {
 *         <root domain>: {
 *             <resourceURLPath>: '<integrity digests, separated by spaces>'
 *         }
 *         ...
 *     },
 *     expirations: [ // ordered by ascending order of .time
 *         {
 *             domain: <root domain>,
 *             time: <UNIX time in milliseconds>
 *         },
 *         ...
 *     ]
 * }
 *
 */
'use strict';
const extractRootDomain = require('./name').extractRootDomain;
const removePrefix = require('./name').removePrefix;
const removeQueryAndHash = require('./name').removeQueryAndHash;

class Cache {
    constructor() {
        if (localStorage.getItem('domainHashes') && localStorage.getItem('domainExpirations')) {
            this.domains = JSON.parse(localStorage.getItem('domainHashes'));
            this.expirations = JSON.parse(localStorage.getItem('domainExpirations'));
            this.insecureDomains = new Set(JSON.parse(localStorage.getItem('insecureDomains')));
        } else {
            this.domains = {};
            this.expirations = [];
            this.insecureDomains = new Set();
            this.commit();
        }
    }

    getDomains() {
        return Object.keys(this.domains);
    }

    getHashes(url, initiator = undefined) {
        const root = extractRootDomain(initiator || url);
        if (this.domains[root]) {
            const cleanUrl = removeQueryAndHash(removePrefix(url));
            return this.domains[root][cleanUrl];
        } else {
            return undefined;
        }
    }

    isWhitelisted(url, initiator = undefined) {
        const root = extractRootDomain(initiator || url);
        const cleanUrl = removeQueryAndHash(removePrefix(url));
        if (this.domains[root]) {
            return this.domains[root].whitelist.some(url => {
                if (url.slice(-2) === '/*') {
                    return cleanUrl.startsWith(url.slice(0, -1));
                } else {
                    return url === cleanUrl;
                }
            });
        } else {
            return false;
        }
    }

    get(domain) {
        return this.domains[domain];
    }

    getExpiredDomains() {
        const firstUnexpiredIndex = this.expirations.findIndex(
            expiration => expiration.time > Date.now()
        );
        return this.expirations.slice(0, firstUnexpiredIndex).map(obj => obj.domain);
    }

    getInsecureDomains() {
        return this.insecureDomains;
    }

    removeInsecure(domain) {
        this.insecureDomains.delete(domain);
        this.commit();
    }

    removeDomain(domain) {
        delete this.domains[domain];
        this.expirations.splice(
            this.expirations.findIndex(expiration => expiration.domain === domain),
            1
        );
        this.commit();
    }

    set(domain, hashes) {
        if (this.domains[domain]) {
            this.removeDomain(domain);
        }
        this.domains[domain] = hashes;
        this.expirations.push({
            domain,
            time: hashes.expiration
        });
        this.expirations.sort((a, b) => a.time - b.time);
        // expirations is sorted in ascending order
        this.commit();
    }

    setInsecure(domain) {
        this.insecureDomains.add(domain);
        this.commit();
    }

    clear() {
        this.domains = {};
        this.commit();
    }

    includes(domain) {
        return this.get(domain) !== undefined;
    }

    excludes(domain) {
        return this.get(domain) === undefined;
    }

    isInsecure(domain) {
        return this.insecureDomains.has(domain);
    }

    commit() {
        localStorage.setItem('domainHashes', JSON.stringify(this.domains));
        localStorage.setItem('domainExpirations', JSON.stringify(this.expirations));
        localStorage.setItem('insecureDomains', JSON.stringify([...this.insecureDomains]));
    }
}

module.exports = Cache;
