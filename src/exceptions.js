'use strict';


function DnsOverHttpsApiException(message) {
    this.message = message;
    this.name = 'DnsOverHttpsApiException';
}

function DnsQueryException(message) {
    this.message = message;
    this.name = 'DnsQueryException';
}

function ValidationFailureException(message) {
    this.message = message;
    this.name = 'ValidationFailureException';
}

function NotImplementedException(message) {
    this.message = message;
    this.name = 'NotImplementedException';
}

function OutdatedExtensionException(message) {
    this.message = message;
    this.name = 'OutdatedExtensionException';
}

module.exports = {
    DnsOverHttpsApiException,
    DnsQueryException,
    NotImplementedException,
    OutdatedExtensionException,
    ValidationFailureException
};
