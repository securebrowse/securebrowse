/**
 * The persistent background script in the extension that ties it all together.
 * Used to interface with dashboard.js, settings.js, and document_start.js script to display security notifications, and call verification functions
 */
'use strict';
const Alerts = require('./alerts');
const Cache = require('./cache');
const dnssec = require('./dnssec');
const dnsUtils = require('./dns-utils');
const extractRootDomain = require('./name').extractRootDomain;
// const fuzzyMatch = require('./fuzzymatch');
const removePrefix = require('./name').removePrefix;
const removeQueryAndHash = require('./name').removeQueryAndHash;
const OutdatedExtensionException = require('./exceptions').OutdatedExtensionException;
const ValidationFailureException = require('./exceptions').ValidationFailureException;
const utils = require('./background-utils');


const tabStatuses = {outdated: false};
const tabPageResources = {};
const cache = new Cache();
const alerts = new Alerts(tabStatuses);

localStorage.setItem('expirationTime', 0);
localStorage.setItem('response', '');
window.browser = window.msBrowser || window.browser || window.chrome;


async function validateDomain(domain) {
    /**
     * Verifies the DNSSEC status and SecureBrowse TXT records of the domain. If it is verified,
     * this returns an object of hashRecords. The hashRecords consist of key-value pairs:
     *     urlPath of file -- integrity
     * This function is called after a user adds a domain to verify in the settings page
     * @param {string} domain
     * @return {Promise<Object>} hashRecords
     */
    domain = extractRootDomain(domain);
    try {
        var txtRRSet = await dnsUtils.getRRSet(domain, 'TXT');

        const secureBrowseVersion = utils.getSecureBrowseVersion(txtRRSet);
        if (secureBrowseVersion === 'NOT_FOUND') {
            throw new ValidationFailureException('The domain is missing a SecureBrowse TXT record.');
        } else if (Math.floor(Number(secureBrowseVersion)) > 1) {
            throw new OutdatedExtensionException();
        }

        var [dnssecStatus, ] = await dnssec.validateDnssec(domain, ['TXT']);
    } catch (e) {
        console.error(e);
        throw e;
    }

    if (dnssecStatus.TXT !== 1) {
        throw new ValidationFailureException('The domain has DNSSEC issues or is not DNSSEC signed.');
    }

    return utils.buildSRIHashRecordFromTxtRRSet(txtRRSet);
}

async function validatePage(tabId, domain) {
    /**
     * Verifies the DNSSEC status, SecureBrowse TXT records, & resources of a page the user visits.
     * This function is called after a user visits a webPage that isn't added as a domain to verify
     * in the settings page
     * @param {Number} tabId the id of the tab the page is on
     * @param {string} domain
     */
    if (!domain.startsWith('http')) {
        return;
    }
    domain = extractRootDomain(domain);
    let create = true;

    // if (fuzzyMatch(domain, cache.getDomains())) {
    //     alerts.reportPhishingDetected(tabId, domain);
    //     create = false;
    // }

    try {
        var txtRRSet = await dnsUtils.getRRSet(domain, 'TXT');
    } catch (e) {
        alerts.reportConnectionError(tabId, domain);
        return;
    }

    const secureBrowseVersion = utils.getSecureBrowseVersion(txtRRSet);
    if (secureBrowseVersion === 'NOT_FOUND') {
        return;
    } else if (Math.floor(Number(secureBrowseVersion)) > 1) {
        alerts.reportExtensionNeedsUpdate(tabId, domain);
        return;
    }

    try {
        var [dnssecStatus, ] = await dnssec.validateDnssec(domain, ['TXT']);
    } catch (e) {
        alerts.reportUnsafeResource(tabId, domain, 'all', false, create);
        return;
    }

    if (dnssecStatus.TXT !== 1) {
        alerts.reportUnsafeResource(tabId, domain, 'all', false, create);
        return;
    }

    let hashRecord = utils.buildSRIHashRecordFromTxtRRSet(txtRRSet);
    let resources = tabPageResources[tabId];
    delete tabPageResources[tabId];

    try {
        var resourceContents = await Promise.all(
            resources.map(resource => utils.requestFileContentAsync(resource.url, resource.type))
        );
    }
    catch (err) {
        alerts.reportUnsafeResource(tabId, domain, err.type, false, create);
    }
    for (let i = 0; i < resourceContents.length; i++) {
        const cleanUrl = removePrefix(removeQueryAndHash(resources[i].url));
        const isWhitelisted = hashRecord.whitelist.some(url => {
            if (url.slice(-2) === '/*') {
                return cleanUrl.startsWith(url.slice(0, -1));
            } else {
                return url === cleanUrl;
            }
        });

        if (isWhitelisted) {
            alerts.reportSafeResource(tabId, domain, resources[i].type, create);
            continue;
        }

        const hashes = hashRecord[cleanUrl].split(' ');
        const matchFound = utils.checkForMatch(hashes, resourceContents[i]);

        if (matchFound) {
            alerts.reportSafeResource(tabId, domain, resources[i].type, create);
        } else {
            alerts.reportUnsafeResource(tabId, domain, resources[i].type, false, create);
            break;
        }
    }

    alerts.setIcon(tabId);
}

function resourceIsNotInCache(details) {
    /**
     * Determines whether a request resource is found in the hash cache
     * @param {Object} details Details about the request. It's an object given to the onBeforeRequest
     * listener
     * @return {boolean}
     */
    if (!details.url.startsWith('http')) {
        return true;
    }
    // Request type must be of type: script, stylesheet, main_frame
    if (!['script', 'stylesheet', 'main_frame'].includes(details.type)) {
        return true;
    }
    // Check if cache does not contain the URL of the document being loaded
    if (details.type === 'main_frame' && cache.excludes(extractRootDomain(details.url))) {
        return true;
    }
    // Check if cache does not contain the URL of the document in which the resource will be loaded
    if (['script', 'stylesheet'].includes(details.type) &&
        cache.excludes(extractRootDomain(details.initiator || details.documentUrl))) {
        return true;
    }
    return false;
}

function generateBlockingResponse(details, root) {
    /**
     * Create a return BlockingResponse object for webRequest event handlers. Either it'll redirect
     * a request if the request is for a main_frame, or block it entirely.
     * @param {Object} details
     * @param {string} root
     * @return {Object}
     */
    if (details.type === 'main_frame') {
        const redirectUrl = browser.extension.getURL('blocked.html') +
        `?sbRoot=${window.btoa(root)}&sbUrl=${window.btoa(details.url)}`;
        return {redirectUrl};
    } else {
        return {cancel: true};
    }
}

function saveForVerification(tabId, url, type) {
    /**
     * Temporarily saves a url & its resource type to tabPageResources. Once a tab has finished loading
     * and we detect that it is SecureBrowse-enabled, we verify all the resources in these urls
     * @param {Number} tabId
     * @param {string} url
     * @param {type} type
     */
    if (!['script', 'stylesheet', 'main_frame'].includes(type)) {
        return;
    } else if (tabId in tabPageResources) {
        tabPageResources[tabId].push({url, type});
    } else {
        tabPageResources[tabId] = [{url, type}];
    }
}

browser.webRequest.onBeforeRequest.addListener((details) => {
    const root = extractRootDomain(details.url);
    if (cache.isInsecure(root)) {
        alerts.reportUnsafeResource(details.tabId, root, details.type);
        return generateBlockingResponse(details, root);
    }
    if (resourceIsNotInCache(details)) {
        if (details.type === 'main_frame') {
            alerts.disableSbTab(details.tabId, details.url);
        }
        saveForVerification(details.tabId, details.url, details.type);
        return {cancel: false};
    }

    delete tabPageResources[details.tabId];

    let sourceUrl = details.initiator || details.documentUrl;
    if (details.type === 'main_frame') {
        sourceUrl = details.url;
    }

    if (cache.isWhitelisted(details.url, sourceUrl)) {
        alerts.reportSafeResource(details.tabId, root, details.type);
        return {cancel: false};
    }

    const hashes = cache.getHashes(details.url, sourceUrl);
    if (hashes === undefined) {
        alerts.reportUnsafeResource(details.tabId, root, details.type);
        return generateBlockingResponse(details, root);
    }

    try {
        var content = utils.requestFileContentSync(details.url);
    } catch (e) {
        alerts.reportUnsafeResource(details.tabId, root, details.type);
        console.error(e);
        return generateBlockingResponse(details, root);
    }

    const matchFound = utils.checkForMatch(hashes.split(' '), content);
    if (matchFound) {
        alerts.reportSafeResource(details.tabId, root, details.type);
        return {cancel: false};
    } else {
        alerts.reportUnsafeResource(details.tabId, root, details.type);
        return generateBlockingResponse(details, root);
    }
}, {urls: ['<all_urls>']}, ['blocking']);

browser.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    // TODO: Design a yellow loading icon to indicate that the tab is not done loading & SB is still
    // verifying the page
    // ^It would need to be set in the onBeforeRequest listener & here for .status === 'loading'
    if (changeInfo.status === 'complete') {
        if (tab.url.startsWith(browser.extension.getURL('blocked.html')) ||
            !(tabId in tabPageResources)) {
            alerts.setIcon(tabId);
            return;
        }
        validatePage(tabId, tab.url);
    }
});

browser.tabs.onRemoved.addListener((tabId) => {
    delete tabStatuses[tabId];
});

// Handle messages from dashboard.js and settings.js
browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
    function sendVerificationResponseToSettingsTab(secure, error = undefined) {
        browser.tabs.sendMessage(sender.tab.id, {
            type: 'settings',
            domain: request.domain,
            secure,
            error
        });
    }

    switch (request.type) {
    case 'tabInfo': {
        if (request.tabID in tabStatuses) {
            sendResponse(tabStatuses[request.tabID]);
        } else {
            sendResponse({
                sbEnabled: false,
                url: extractRootDomain(request.tabURL)
            });
        }
        return;
    }
    case 'verify': {
        validateDomain(request.domain)
            .then((hashes) => {
                cache.set(request.domain, hashes);
                sendVerificationResponseToSettingsTab(true);
            })
            .catch((error) => {
                cache.setInsecure(request.domain);
                sendVerificationResponseToSettingsTab(false, error);
            });
        return;
    }
    case 'getCachedDomains': {
        sendResponse({
            secureDomains: cache.getDomains(),
            insecureDomains: [...cache.getInsecureDomains()]
        });
        return;
    }
    case 'removeDomain': {
        cache.removeDomain(request.domain);
        return;
    }
    case 'removeInsecureDomain': {
        cache.removeInsecure(request.domain);
        return;
    }
    }
});

browser.alarms.create('verifyExpiredDomainsInCache', {periodInMinutes: 1});
browser.alarms.create('reverifyInsecureDomains', {periodInMinutes: 1});

browser.alarms.onAlarm.addListener((alarm) => {
    if (alarm.name === 'verifyExpiredDomainsInCache') {
        cache.getExpiredDomains()
            .forEach((domain) => {
                cache.removeDomain(domain);
                validateDomain(domain)
                    .then((hashes) => {
                        cache.set(domain, hashes);
                    });
            });
    } else if (alarm.name === 'reverifyInsecureDomains') {
        cache.getInsecureDomains()
            .forEach((domain) => {
                validateDomain(domain)
                    .then((hashes) => {
                        cache.set(domain, hashes);
                        cache.removeInsecure(domain);
                    });
            });
    }
});
