/**
 * The dns utils script contains some useful functions for getting DNS records. It defaults on
 * Cloudflare's DNS-over-HTTPS API.
 */
'use strict';
const Name = require('./name').Name;
const RData = require('./rdata');
const RRset = require('./rrset').RRset;
const DnsOverHttpsApiException = require('./exceptions').DnsOverHttpsApiException;
const DnsQueryException = require('./exceptions').DnsQueryException;
const ValidationFailureException = require('./exceptions').ValidationFailureException;

const cloudFlareDnsOverHttps = 'https://cloudflare-dns.com/dns-query?';


function dnsQuery(name, rdtype, dnssecOk=false) {
    /**
     * Requests the DNS-over-HTTPS API for records of type rdtype
     * @param {string} name A FQDN URL string
     * @param {string} rdtype A resource record type represented by letters
     * @return {Promise} The API response
     */
    return new Promise((resolve, reject) => {
        if (name === '.' && rdtype === 'DNSKEY' && Date.now() < Number(localStorage.expirationTime)) {
            resolve(JSON.parse(localStorage.response));
            return;
        }

        const apiURL = cloudFlareDnsOverHttps;
        let formatQueryString = paramsObject => {
            return Object
                .keys(paramsObject)
                .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(paramsObject[key])}`)
                .join('&');
        };
        const params = {
            name: name,
            type: rdtype,
            do: dnssecOk
        };
        const urlQueryString = apiURL + formatQueryString(params);
        fetch(urlQueryString, {
            method: 'GET',
            headers:{
                'Accept': 'application/dns-json',
                'pragma': 'no-cache',
                'cache-control': 'no-cache',
            }
        }).then(res => res.ok ? res : Promise.reject({status: res.status, res}))
            .then(response => {
                resolve(
                    response.json().then(data => {
                        if (name === '.' && rdtype === 'DNSKEY') {
                            localStorage.setItem('expirationTime', data.Answer[0].TTL * 1000 + Date.now());
                            localStorage.setItem('response', JSON.stringify(data));
                        }
                        return data;
                    })
                );
            })
            .catch(error => reject(error));
    });
}

async function getDnsResponses(domainNames) {
    /**
     * Sends multiple dnsQuery requests to zone and parent zones. Checks for server API errors.
     * @param {array} domainNames an array of zones to fetch records from. Usually generated
     * from Name.getAllNames()
     * @return {Promise} An array of DNS-over-HTTPS API responses
     */
    let promises = [];
    domainNames.forEach(domainName => {
        promises.push(dnsQuery(domainName, 'DS', true));
        promises.push(dnsQuery(domainName, 'TXT', true));
        promises.push(dnsQuery(domainName, 'DNSKEY', true));
    });
    return Promise.all(promises)
        .then(responses => {
            for (let res of responses) {
                if (!res.AD || res.CD) {
                    throw new ValidationFailureException('DNS-over-HTTPS validation failure.');
                }
                if (res.Status !== 0 || res.TC || !res.RD || !res.RA) {
                    throw new DnsQueryException('Unexpected response.');
                }
            }
            return responses;
        })
        .catch(error => {
            throw new DnsOverHttpsApiException(error);
        });
}

function insertRDataFromAnswer(answer, obj) {
    /**
     * Inserts RR data from API Answer into a new RData object. Then insert RData into obj by rdtype
     * @param {Object} answer Contains data for one RR
     * @param {Object} obj An object that contains RRs in one zone
     */
    let rr = RData.getRDataFromString(answer.type, answer.TTL, answer.data);
    if (rr === undefined) {
        return obj;
    }
    if (RData.byValue[answer.type] in obj) {
        obj[RData.byValue[answer.type]].list.push(rr);
    } else {
        obj[RData.byValue[answer.type]] = new RRset(
            Name.fromString(answer.name),
            RData.byValue[answer.type],
            [rr]
        );
    }
    return obj;
}

async function getRRSet(name, rdtype) {
    let dnsResponse = await dnsQuery(name, rdtype);
    let records = {};
    if (dnsResponse.Answer === undefined) {
        return [];
    }
    for (let i = 0; i < dnsResponse.Answer.length; i++) {
        records = insertRDataFromAnswer(dnsResponse.Answer[i], records);
    }
    return records;
}

async function getDnssecRRSets(name) {
    /**
     * Gets RR data from DNS-over-HTTPS API and reformats it into accessible Javascript objects
     * @param {Name} name The zone to get DNSSEC-related RR data from. Will also retrieve from parent zones
     * @return {Object} An object that contains a subobject for each zone. Each subobject contains their
     * respective RRData objects, organized by rdtype
     */
    try {
        let domainNames = name.getAllNames();
        let dnsResponses = await getDnsResponses(domainNames);
        let dnsRecords = {};
        for (let i = 0; i < dnsResponses.length; i++) {
            const zone = dnsResponses[i].Question[0].name;
            let dnsZoneRecords = dnsRecords[zone] === undefined ? {} : dnsRecords[zone];
            if (dnsResponses[i].Answer === undefined) {
                continue;
            }
            for (let j = 0; j < dnsResponses[i].Answer.length; j++) {
                dnsZoneRecords = insertRDataFromAnswer(dnsResponses[i].Answer[j], dnsZoneRecords);
            }
            dnsRecords[zone] = dnsZoneRecords;
        }
        return dnsRecords;
    }
    catch (e) {
        console.error(e);
    }
}

function getRRSIG(dnsRecords, zone, typeCovered) {
    /**
     * Gets RRset from the object returned from getDnssecRRSets()
     * @param {Object} dnsRecords See getDnssecRRSets()
     * @param {string} zone The Zone name RRset is in
     * @param {string} typeCovered The resource record type covered in lettered format
     * @return {RRSIG} The RRSIG
     */
    return dnsRecords[zone].RRSIG.list.filter(r => r.typeCovered === typeCovered)[0];
}

module.exports = {
    getDnssecRRSets,
    getRRSIG,
    getRRSet
};
