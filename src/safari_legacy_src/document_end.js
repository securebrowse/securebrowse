/**
 * This script is inserted into the end of every web page the user visits.
 * Used to send files (scripts, css files) to background.js when requested to.
 * Also relays messages to user if anything insecure is detected.
 */
'use strict';

function removePrefix(url) {
    return url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, '');
}

function convertElementForResponse(element, response, hashRecords, attr, isSubResourceIntegritySupported) {
    if (element.attributes[attr] && ((removePrefix(element[attr]) in hashRecords) ||
        (!isSubResourceIntegritySupported && element.getAttribute('integrity')))) {
        let fileInfo = {
            filePath: element.attributes[attr].nodeValue,
            src: element[attr],
            integrity: element.getAttribute('integrity'),
            inRecord: removePrefix(element[attr]) in hashRecords,
            attr
        };
        response.push(fileInfo);
    }
}

safari.self.addEventListener('message', (msgEvent) => {
    if (msgEvent.name === 'getFiles') {
        let hashRecords = msgEvent.message.hashRecords;
        let response = [];
        let scripts = Array.from(document.getElementsByTagName('script')).filter(script => {
            return script.src !== safari.extension.baseURI + '/canary.js';
        });
        for (let i = 0; i < scripts.length; i++) {
            convertElementForResponse(scripts[i], response, hashRecords, 'src', msgEvent.message.isSubResourceIntegritySupported);
        }
        if (msgEvent.message.setting.secureCSS) {
            var sheets = document.getElementsByTagName('link');
            for (let i = 0; i < sheets.length; i++) {
                convertElementForResponse(sheets[i], response, hashRecords, 'href', msgEvent.message.isSubResourceIntegritySupported);
            }
        }
        if (msgEvent.message.setting.panic) {
            let numFiles = scripts.length + (msgEvent.message.setting.secureCSS ? sheets.length : 0);
            // On panic mode, a web page that has any scripts that don't have a matching TXT record
            // are insecure
            if (numFiles > Object.keys(hashRecords).length) {
                response = 'Insecure files detected. Please check SecureBrowse';
            }
        }
        safari.self.tab.dispatchMessage('receiveFiles', {
            hashRecords,
            domainName: msgEvent.message.domainName,
            files: response
        });
    }
}, false);
