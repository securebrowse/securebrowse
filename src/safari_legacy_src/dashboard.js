/**
 * This script manipulates the extension dashboard popup html UI with information about the page's
 * security status.
 * Upon opening the popup, this script is run and sends a request to background.js to get security
 * data about the web page the user is on.
 */
'use strict';
const check = './images/GoodIcon.svg';
const x = './images/BadIcon.svg';

function el(id) {
    return document.getElementById(id);
}

// Create list detail item and append to <ul> element
function addListItem(obj) {
    if (obj.amt === 0) {
        return;
    }
    let name = obj.amt === 1 ? obj.name.slice(0, -1) : obj.name;
    let text = '';
    if (obj.secure) {
        text = obj.amt + ' ' + name + ' Verified';
    } else {
        text = obj.amt + ' ' + name + ' Insecure';
    }
    el('info').innerHTML += li(
        obj.secure ? 'shield-safe' : 'shield-unsafe',
        text);
}

// Return template string of <li> element with class and text
function li(cls, txt) {
    return `<li>
        <span class="${cls}"></span>
        <span>${txt}</span>
    </li>`;
}

function clearDashboard() {
    el('hero').firstChild.src = './images/Icon.svg';
    el('explanation').style.display = 'None';
    el('info').innerHTML = '';
}

// Update dashboard according to response object (r)
// See response obj in background.js
function updateDashboard(r) {
    clearDashboard();
    el('url').innerHTML = r.url;
    if (r.active) {
        // The web page fuzzy-matches to an expected domain
        if (r.scripts.secure && r.style_sheets.secure && r.phishingSecure) {
            // Web Page Verified
            el('hero').firstChild.src = check;
            el('status').innerHTML = 'WEB PAGE VERIFIED';
        } else {
            // Web Page Insecure
            el('hero').firstChild.src = x;
            el('status').innerHTML = 'WEB PAGE INSECURE';
            let dialog = '';
            if (!r.phishingSecure) {
                dialog = `Looks like you're on the wrong website.
                Make sure that the URL is actually the correct one.`;
            } else if (r.needToUpdate) {
                dialog = 'New version of SecureBrowse security protocol detected. Have you updated your plugin?';
            } else {
                dialog = 'There are insecure scripts loaded. Attackers might be trying to steal your information.';
            }
            el('explanation').innerHTML = dialog;
            el('explanation').style.display = '';
        }
        addListItem(r.scripts);
        addListItem(r.style_sheets);
        if ('html' in r) {
            addListItem(r.html);
        }
        el('info').innerHTML += li(
            'phishing-' + (r.phishingSecure ? 'safe' : 'unsafe'),
            (r.phishingSecure ? 'No ' : '') + 'Mispelling/Phishing Detected'
        );
    } else {
        el('status').innerHTML = 'Verification Not Enabled';
    }
}
