/**
 * The persistent background script in the extension that ties it all together.
 * Used to interface with dashbord.js, document_start.js, and document_end.js script to display security notifications, and
 * request files for SRI & DNSSEC verification. Based off of ../background.js & rewritten for Safari extensions
 */
'use strict';
const buildSRIHashRecordFromTxtRRSet = require('../background-utils').buildSRIHashRecordFromTxtRRSet;
const dnssec = require('../dnssec');
const dnsUtils = require('../dns-utils');
const extractRootDomain = require('../name').extractRootDomain;
const fuzzyMatchURL = require('../fuzzymatch').fuzzyMatchURL;
const getSecureBrowseVersion = require('../background-utils').getSecureBrowseVersion;
const response = require('../background-utils').response;
const sriFilesCheck = require('../background-utils').sriFilesCheck;

let isSubResourceIntegritySupported = false;
// This object keeps track of the security status for multiple tabs.
// The keys are the tabIds, and the values are copies of the response obj above.
let tabStatuses = new Map();

localStorage.setItem('expirationTime', 0);
localStorage.setItem('response', '');

function setupInsecureResponse(message, tab, hostname, phishingSecure = true, data, needToUpdate) {
    /**
     * Updates tabStatuses map with security status of a tab and notifies the user that the web
     * page is insecure.
     * @param {string} message The message used to notify the user
     * @param {object} tab Safari message target. The tab we're communicating to.
     * @param {string} hostname domain name of web page
     * @param {bool} phishingSecure true if there is no phishing detected. false otherwise.
     * @param {object} data miscellaneous information on how many script or css files are secure
     * or insecure
     * @param {bool} needToUpdate Used to indicate whether there is a new SecureBrowse protocol
     */
    // Clone response object and insert the clone into tabStatuses map at the corresponding tabId
    tabStatuses.set(tab, Object.assign({}, response));
    let status = tabStatuses.get(tab);
    status.active = true;
    status.phishingSecure = phishingSecure;
    status.url = hostname;
    status.needToUpdate = needToUpdate;
    if (data === 'ALL') {
        status.scripts.secure = false;
        status.style_sheets.secure = false;
        status.scripts.amt = 'All';
        status.style_sheets.amt = 'All';
    } else {
        Object.assign(status, data);
    }
    tab.activate();
    setTimeout(() => {
        safari.extension.toolbarItems.find(
            toolbarItem => toolbarItem.browserWindow.tabs.includes(tab)
        ).showPopover();
    }, 200);

}

function setupSecureResponse(tab, hostname, data) {
    /**
     * Updates tabStatuses map with security status of a tab. Other than the secure icon update,
     * no other notifications are shown since the web page is secure.
     * @param {object} tab Safari message target. The tab we're communicating to.
     * @param {string} hostname domain name of web page
     * @param {object} data miscellaneous information on how many script or css files are secure
     * or insecure
     */
    // Clone response object and insert the clone into tabStatuses at the corresponding tabId
    tabStatuses.set(tab, Object.assign({}, response));
    let status = tabStatuses.get(tab);
    status.active = true;
    status.url = hostname;
    if (data !== undefined) {
        Object.assign(status, data);
    }
}

function setupSecurityResponse(message, tab, domainName,
    numScripts, numSheets, numBadScripts, numBadSheets,
    documentHtmlSecurity) {
    /**
     * Sends out appropriate security notifications to user depending on whether an insecure files
     * exist. See verifyWebPageIfURLMatches().
     */
    if (numBadScripts > 0 || numBadSheets > 0 || documentHtmlSecurity === -1) {
        let data = {
            scripts: {
                name: 'Scripts',
                secure: numBadScripts === 0,
                amt: numBadScripts === 0 ? numScripts : numBadScripts
            },
            style_sheets: {
                name: 'Style Sheets',
                secure: numBadSheets === 0,
                amt: numBadSheets === 0 ? numSheets : numBadSheets
            }
        };
        if (documentHtmlSecurity !== 0) {
            data['html'] = {
                name: 'HTML files',
                secure: documentHtmlSecurity === 1,
                amt: 1
            };
        }
        setupInsecureResponse(message, tab, domainName, true, data);
    } else {
        let data = {
            scripts: {
                name: 'Scripts',
                secure: true,
                amt: numScripts
            },
            style_sheets: {
                name: 'Style Sheets',
                secure: true,
                amt: numSheets
            },
        };
        if (documentHtmlSecurity !== 0) {
            data['html'] = {
                name: 'HTML files',
                secure: true,
                amt: 1
            };
        }
        setupSecureResponse(tab, domainName, data);
    }
}

async function verifyWebPageIfURLMatches(tab) {
    /**
     * Determines whether the web page is secure or not. The web page is only secure if:
     *      - no phishing/domain mispelling is detected
     *      - the URL domain is in the set of user-configured expected domains
     *      - files with subresource integrity pass verification
     *      - the SRI digests match the TXT records' values of the domain
     * The function is called every time the user navigates to a new page. The extension sends out
     * notifications to the user depending on the security status of the page.
     * @param {object} tab Safari message target. The tab we're communicating to.
     */
    const domainName = extractRootDomain(tab.url);
    // Update security status to neutral state because we are redoing the verification of a tab
    tabStatuses.delete(tab);
    let domains = safari.extension.settings.domains
        .toLowerCase()
        .replace(/ /g,'')
        .replace(/(^,)|(,$)/g, '')
        .split(',')
        .filter(item => item !== '');
    if (fuzzyMatchURL(domainName, domains)) {
        const message = 'SecureBrowse: Mispelling/Phishing Detected. Check the URL.';
        setupInsecureResponse(message, tab, domainName, false);
        return;
    }
    const message = 'Insecure files detected. Please check SecureBrowse.';
    try {
        var txtRRSet = await dnsUtils.getRRSet(domainName, 'TXT');
    } catch (e) {
        let err_message = message;
        if (!domains.includes(domainName)) {
            err_message = 'Error: SecureBrowse is unable to connect to the DNS-over-HTTPS API.';
        }
        setupInsecureResponse(err_message, tab, domainName, true, 'ALL', false);
        console.error(e);
        return;
    }
    // let txtRRSet = await dnsUtils.getRRSet(domainName, 'TXT');
    const secureBrowseVersion = getSecureBrowseVersion(txtRRSet);
    if (secureBrowseVersion === 'NOT_FOUND') {
        if (domains.includes(domainName)) {
            setupInsecureResponse(message, tab, domainName, true, 'ALL', false);
        }
        return;
    } else if (Math.floor(Number(secureBrowseVersion)) > 1) {
        const message = 'New version of SecureBrowse security protocol detected. Have you updated your plugin?';
        setupInsecureResponse(message, tab, domainName, true, 'ALL', true);
        return;
    }

    if (!domains.includes(domainName)) {
        safari.extension.settings.domains = safari.extension.settings.domains + ', ' + domainName;
    }

    // Validate DNSSEC
    try {
        var [dnssecStatus, dnsRecords] = await dnssec.validateDnssec(domainName, ['TXT']);
    } catch (e) {
        setupInsecureResponse(message, tab, domainName, true, 'ALL');
        return;
    }

    if (dnssecStatus.TXT !== 1) {
        setupInsecureResponse(message, tab, domainName, true, 'ALL');
        return;
    }
    // DNSSEC VERIFICATION PASSES. Now check for SRI and comparing integrity values
    // against TXT record values

    // get TXT records.
    // We assume all the TXT records of the domain have integrity hash digests
    // let txtRRSet = await dnsUtils.getRRSet(domainName, 'TXT');
    // Filter out any TXT records that don't contain SRI hashes
    txtRRSet.TXT.list = txtRRSet.TXT.list.filter((txt) => {
        return ['sha256-', 'sha384-', 'sha512-'].some(substr => txt.getString().includes(substr));
    });
    // insert them into an object
    let hashRecords = buildSRIHashRecordFromTxtRRSet(txtRRSet);
    // request script and CSS files from the web page
    tab.page.dispatchMessage('getFiles', {
        hashRecords,
        isSubResourceIntegritySupported,
        setting: {
            secureCSS: safari.extension.settings.secureCSS,
            panic: safari.extension.settings.panic
        }
    });
}

safari.extension.settings.addEventListener('change', (changeEvent) => {
    if (changeEvent.key === 'domains' && changeEvent.oldValue !== changeEvent.newValue) {
        const domains = safari.extension.settings.domains + '';
        let domainArray = domains
            .toLowerCase()
            .replace(/ /g,'')
            .replace(/(^,)|(,$)/g, '')
            .split(',')
            .filter(item => item !== '');
        safari.extension.settings.domains = domainArray.join(', ');
    }
}, false);

safari.application.addEventListener('popover', (popEvent) => {
    const activeTab = safari.application.activeBrowserWindow.activeTab;
    const tabStatus = tabStatuses.get(activeTab) || response;

    popEvent.target.contentWindow.updateDashboard(tabStatus);
    const style = popEvent.target.contentWindow.el('dashboard').style;
    style.display = 'block';
    style.opacity = 1;
}, true);

// Listen for requests from document_end.js & document_start.js
safari.application.addEventListener('message', async (msgEvent) => {
    if (msgEvent.name === 'receiveFiles') {
        let files = msgEvent.message.files;
        let domainName = extractRootDomain(msgEvent.target.url);
        let hashRecords = msgEvent.message.hashRecords;
        if (files === 'Insecure files detected. Please check SecureBrowse') {
            setupInsecureResponse(files, msgEvent.target, domainName, true, 'ALL');
            return;
        }
        let [numScripts, numSheets,
            numBadScripts, numBadSheets,
            documentHtmlSecurity
        ] = await sriFilesCheck(
            isSubResourceIntegritySupported, msgEvent.target.url, hashRecords, files
        );

        const message = 'Insecure files detected. Please check SecureBrowse.';
        setupSecurityResponse(message, msgEvent.target, domainName,
            numScripts, numSheets, numBadScripts, numBadSheets,
            documentHtmlSecurity);
    } else if (msgEvent.name === 'startVerification') {
        msgEvent.target.addEventListener('close', (closeEvent) => {
            // A tab that contains a web page we're keeping a security status on has been closed.
            tabStatuses.delete(closeEvent.target);
        }, false);

        isSubResourceIntegritySupported = msgEvent.message.isSubResourceIntegritySupported;
        if (msgEvent.message.numSriFailedFiles.some(num => num > 0)) {
            setupSecurityResponse(
                'Some files have failed SRI validation. Attackers might be trying to steal your information. Please check SecureBrowse.',
                msgEvent.target,
                extractRootDomain(msgEvent.target.url),
                0,
                0,
                msgEvent.message.numSriFailedFiles[0],
                safari.extension.settings.secureCSS ? msgEvent.message.numSriFailedFiles[1] : 0,
                0
            );
        } else {
            verifyWebPageIfURLMatches(msgEvent.target);
        }
    }
}, false);
