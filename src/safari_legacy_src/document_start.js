/**
 * This script is inserted into the beginning of every web page the user visits.
 * Used to detect any SRI digest verification failures in the page and send the amt
 * of failed files to background.js. Also tests whether the browser supports SubResource Integrity
 * by appending a canary.js script to the document. If an SRI verification failure is detected from
 * the canary, then SRI is supported. Otherwise it is unsupported.
 */
'use strict';
let numSriFailedFiles = [0, 0];
let isSubResourceIntegritySupported = false;
const canaryURL = safari.extension.baseURI + '/canary.js';

function processNode(node) {
    const tagName = (node.tagName || '').toLowerCase();
    if ((tagName === 'link' || tagName === 'script') && node.getAttribute('integrity')) {
        node.onerror = e => {
            if (node.src === canaryURL) {
                isSubResourceIntegritySupported = true;
            } else {
                numSriFailedFiles[tagName === 'script' ? 0 : 1]++;
            }
        }
    }
}

function documentReady(fn) {
    if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

new MutationObserver(mutations => {
    mutations.forEach(mutation => {
        mutation.addedNodes.forEach(processNode);
    });
}).observe(document, {childList: true, subtree: true});

let canaryScript = document.createElement('script');
canaryScript.src = canaryURL;
canaryScript.type = 'text/javascript';
canaryScript.crossOrigin = 'anonymous';
canaryScript.integrity = 'sha256-vgRElKilQDiwQnf3wW3xgJ/7V1jEGivF9iyYuyLkeE= sha384-xr20yuOki6nTqk1oVnOu3eQFTnPzKxqMjIxZo';
document.documentElement.appendChild(canaryScript);

// Create a verification kickoff function that will only execute once,
// no matter how many times it is called
let startVerificationOnce = (() => {
    let executed = false;
    return () => {
        if (!executed) {
            executed = true;
            setTimeout(() => {
                safari.self.tab.dispatchMessage(
                    'startVerification',
                    {numSriFailedFiles, isSubResourceIntegritySupported}
                );
            }, 300);
        }
    };
})();

documentReady(() => {
    if (document.visibilityState === 'prerender') {
        document.addEventListener('visibilitychange', () => {
            startVerificationOnce();
        });
    } else {
        startVerificationOnce();
    }
});
