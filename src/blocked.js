/**
 * This script is used in blocked.html to add the blocked page url strings to the page.
 */
'use strict';

const url = new URL(document.URL);

function updateText(id, param) {
    const text = window.atob(url.searchParams.get(param));
    document.getElementById(id).innerText = text;
}

updateText('root', 'sbRoot');
updateText('url', 'sbUrl');
