/**
 * This script is inserted into the beginning of every web page the user visits.
 * Used to relay alert messages from alerts.js about insecure issues.
 */
'use strict';
window.browser = window.msBrowser || window.browser || window.chrome;

browser.runtime.onMessage.addListener((request) => {
    if (request.type === 'alert') {
        alert(request.message);
    }
});
