/**
 * The dnssec script deals with full DNSSEC chain validation for a RRSet in a zone.
 */
// 'use strict';
const crypto = require('crypto');
const EllipticCurve = require('elliptic').ec;
const jsPack = require('jspack').jspack;
const nodeRsa = require('node-rsa');
const dnsUtils = require('./dns-utils');
const Name = require('./name').Name;
const RData = require('./rdata');
const NotImplementedException = require('./exceptions').NotImplementedException;
const ValidationFailureException = require('./exceptions').ValidationFailureException;


const RSAMD5 = 1;
const DH = 2;
const DSA = 3;
const RESERVED = 4;
const RSASHA1 = 5;
const DSANSEC3SHA1 = 6;
const RSASHA1NSEC3SHA1 = 7;
const RSASHA256 = 8;
const RSASHA512 = 10;
const ECDSAP256SHA256 = 13;
const ECDSAP384SHA384 = 14;
const INDIRECT = 252;
const PRIVATEDNS = 253;
const PRIVATEOID = 254;

const NOT_FOUND = 0;
const INSECURE = -1;
const SECURE = 1;

const algorithmsByText = {
    RSAMD5,
    DH,
    DSA,
    RESERVED,
    RSASHA1,
    DSANSEC3SHA1,
    RSASHA1NSEC3SHA1,
    RSASHA256,
    RSASHA512,
    ECDSAP256SHA256,
    ECDSAP384SHA384,
    INDIRECT,
    PRIVATEDNS,
    PRIVATEOID
};

// Flip of algorithmsByText
var algorithmsByValue = {};
for (var key in algorithmsByText) {
    algorithmsByValue[algorithmsByText[key]] = key;
}

function keyId(key) {
    /**
     * Returns the key ID for the specified key
     * @param {DNSKEY} key The specified key
     * @return {number} The key ID
     */
    const rdata = key.toWire();
    if (key.algorithm === RSAMD5) {
        return (rdata.codePointAt(rdata.length - 3) << 8) + (rdata.codePointAt(rdata.length - 2));
    } else {
        let total = 0;
        for (let i = 0; i < Math.floor(rdata.length / 2); i++) {
            total += (rdata.codePointAt(2 * i) << 8) + rdata.codePointAt(2 * i + 1);
        }
        if (rdata.length & 2 !== 0) {
            total += rdata.codePointAt(rdata.length - 1) << 8;
        }
        total += ((total >> 16) & 0xffff);
        return total & 0xffff;
    }
}

function findCandidateKeys(keys, rrsig) {
    /**
     * Filters for possible DNSKEYs that match (via keyTag & algorithm) the RRSIG
     * @param {RRset} keys RRset of DNSKEYs
     * @param {RRSIG} rrsig The RRSIG to reference when searching
     * @return {array} Array of potential DNSKeys. Undefined if none found
     */
    let candidateKeys = keys.list.filter(key => {
        return key.algorithm === rrsig.algorithm && keyId(key) === rrsig.keyTag;
    });
    return candidateKeys.length === 0 ? undefined : candidateKeys;
}

function isRsa(algorithm) {
    return [RSAMD5, RSASHA1, RSASHA1NSEC3SHA1, RSASHA256, RSASHA512].includes(algorithm);
}

function isEcdsa(algorithm) {
    return [ECDSAP256SHA256, ECDSAP384SHA384].includes(algorithm);
}

function isMd5(algorithm) {
    return algorithm === RSAMD5;
}

function isSha1(algorithm) {
    return [DSA, RSASHA1, DSANSEC3SHA1, RSASHA1NSEC3SHA1].includes(algorithm);
}

function isSha256(algorithm) {
    return [RSASHA256, ECDSAP256SHA256].includes(algorithm);
}

function isSha384(algorithm) {
    return algorithm === ECDSAP384SHA384;
}

function isSha512(algorithm) {
    return algorithm === RSASHA512;
}

function makeHash(algorithm) {
    if (isMd5(algorithm)) {
        return 'md5';
    }
    if (isSha1(algorithm)) {
        return 'sha1';
    }
    if (isSha256(algorithm)) {
        return 'sha256';
    }
    if (isSha384(algorithm)) {
        return 'sha384';
    }
    if (isSha512(algorithm)) {
        return 'sha512';
    }
}

function createCurve(algorithm) {
    if (algorithm === ECDSAP256SHA256) {
        const curve = new EllipticCurve('p256');
        const keyLen = 32;
        return [curve, keyLen];
    } else if (algorithm === ECDSAP384SHA384) {
        const curve = new EllipticCurve('p384');
        const keyLen = 48;
        return [curve, keyLen];
    }
}

function validateRRSIG(rrset, rrsig, keys) {
    /**
     * Validates an RRset against a single signature RRSIG
     * @param {array} rrset The RRSET to verify
     * @param {RRSIG} rrsig The signature
     * @param {array} keys The DNSKEYs
     * @return {DNSKEY} The DNSKEY that validates the RRSET if it exists. A ValidationFailureException
     * is thrown otherwise.
     */
    if (rrset.length === 0) {
        throw new ValidationFailureException('RRSET is empty.');
    }
    let candidateKeys = findCandidateKeys(keys, rrsig);
    if (candidateKeys === undefined) {
        throw new ValidationFailureException('Unknown keys.');
    }

    for (let i = 0; i < candidateKeys.length; i++) {
        let candidateKey = candidateKeys[i];
        const now = Date.now() / 1000;
        if (now > rrsig.expiration) {
            throw new ValidationFailureException('RRSIG expired.');
        }
        if (now < rrsig.inception) {
            throw new ValidationFailureException('RRSIG not yet valid.');
        }

        if (isRsa(rrsig.algorithm)) {
            let binaryKey = window.atob(candidateKey.key);
            let [bytes] = jsPack.Unpack('!B', [binaryKey.codePointAt(0)]);
            binaryKey = binaryKey.substr(1);
            if (bytes === 0) {
                // TODO: Haven't found a DNSSEC site that tests this block yet
                [bytes] = jsPack.Unpack('!H', [binaryKey.codePointAt(0), binaryKey.codePointAt(2)]);
                binaryKey = binaryKey.substr(2);
            }
            const rsa_e = binaryKey.substr(0, bytes);
            const rsa_n = binaryKey.substr(bytes);
            let components = {
                e: new Buffer(window.btoa(rsa_e), 'base64'),
                n: new Buffer(window.btoa(rsa_n), 'base64')
            };
            let options = {
                signingScheme: {
                    scheme: 'pkcs1',
                    hash: makeHash(rrsig.algorithm),
                }
            };
            var key = new nodeRsa();
            key.importKey(components, 'components-public');
            key.setOptions(options);
        } else if (isEcdsa(rrsig.algorithm)) {
            let binaryKey = window.atob(candidateKey.key);
            const [curve, keyLen] = createCurve(rrsig.algorithm);
            let x = new Buffer(window.btoa(binaryKey.substr(0, keyLen)), 'base64').toString('hex');
            let y = new Buffer(window.btoa(binaryKey.substr(keyLen, keyLen * 2)), 'base64').toString('hex');
            let pub = {x, y};
            var key = curve.keyFromPublic(pub, 'hex');

            let r = window.btoa(window.atob(rrsig.signature).substr(0, keyLen));
            let s = window.btoa(window.atob(rrsig.signature).substr(keyLen));
            var signature = {
                r: (new Buffer(r, 'base64')).toString('hex'),
                s: (new Buffer(s, 'base64')).toString('hex')
            };
        } else {
            throw new NotImplementedException(`Unknown algorithm ${rrsig.algorithm}`);
        }

        let data = rrsig.toWire().substring(0,18);
        data += rrsig.signer.toWire();
        let rrname = rrset.name;
        let rdataset = rrset.list;
        if (rrsig.labels < rrsig.signer.labels.length - 1) {
            // TODO: Haven't found a DNSSEC site that tests this block yet
            rrname  = rrsig.signer.split(rrsig.labels + 1)[1];
        }
        const rrnameBuf = rrname.toWire();
        const rrfixed = String.fromCodePoint(...jsPack.Pack('!HHI', [rdataset[0].rdtype, 1, rrsig.originalTTL]));
        let rrlist = rdataset.sort(RData.rdataCompare);
        for (let j = 0; j < rrlist.length; j++) {
            data += rrnameBuf;
            data += rrfixed;
            const rrdata = rrlist[j].toWire();
            const rrlen = String.fromCodePoint(...jsPack.Pack('!H', [rrdata.length]));
            data += rrlen;
            data += rrdata;
        }

        if (isRsa(rrsig.algorithm)) {
            if (key.verify(window.btoa(data), rrsig.signature, 'base64', 'base64')) {
                return candidateKey;
            }
        } else if (isEcdsa(rrsig.algorithm)) {
            const digest = crypto.createHash(makeHash(rrsig.algorithm))
                .update(window.btoa(data), 'base64')
                .digest('hex')
                .toUpperCase();
            if (key.verify(digest, signature)) {
                return candidateKey;
            }
        }
    }
    throw new ValidationFailureException('RRSIG Verification Failure');
}

function validateDNSKEY(name, dnskey, ds) {
    /**
     * Verifies a KSK dnskey with a ds
     * @param {Name} name The owner name of the DNSKEY and DS records
     * @param {DNSKEY} dnskey The dnskey to validate
     * @param {DS} ds The ds used to validate the dnskey
     * @return {bool} Returns true if the DNSKEY is validated. A ValidationFailureException is
     * thrown otherwise.
     */
    if (dnskey.algorithm !== ds.algorithm ||
        keyId(dnskey) !== ds.keyTag) {
        throw new ValidationFailureException('DNSKEY and DS do not match');
    }
    let digestType = '';
    if (ds.digestType === 1) {
        digestType = 'sha1';
    } else if (ds.digestType === 2) {
        digestType = 'sha256';
    } else {
        throw new NotImplementedException(`Unknown DS hash digest algorithm ${ds.digestType}`);
    }

    const data = window.btoa(name.toWire() + dnskey.toWire());
    const digest = crypto.createHash(digestType)
        .update(data, 'base64')
        .digest('hex')
        .toUpperCase();
    if (digest === ds.digest) {
        return true;
    } else {
        throw new ValidationFailureException('KSK DNSKEY Verification Failure');
    }
}

function validateRRset(dnsRecords, zone, rdtype) {
    /**
     * Wrapper fn for validateRRSIG(). Finds the rr parameters to verify with.
     * @param {obj} dnsRecords An object that contains a subobject for each zone. Each subobject contains their
     * respective RRData objects, organized by rdtype
     * @param {string} zone The zone the RRSet and RRSIG are held in
     * @param {string} rdtype The type of the RRset to validate
     * @return {string} The zone the DNSKEYset is held in
     */
    let rrset = dnsRecords[zone][rdtype];
    let rrsig = dnsUtils.getRRSIG(dnsRecords, zone, rdtype);
    let dnskeys = dnsRecords[rrsig.signer.toString()].DNSKEY;
    validateRRSIG(rrset, rrsig, dnskeys);
    return dnskeys.name.toString();
}

function validateZoneKSK(dnsRecords, zone) {
    /**
     * Wrapper fn for validateDNSKEY(). Handles orphan DSs and finds the rr parameters to verify with.
     * @param {obj} dnsRecords An object that contains a subobject for each zone. Each subobject contains their
     * respective RRData objects, organized by rdtype
     * @param {string} zone The zone the RRSet and RRSIG are held in
     * @return {string} Output of validateRRSIG()
     */
    let dsList = dnsRecords[zone].DS.list;
    let dnskeys = dnsRecords[zone].DNSKEY.list;
    outerloop:
    for (let i = 0; i < dsList.length; i++) {
        for (let j = 0; j < dnskeys.length; j++) {
            var ds = dsList[i];
            if ([1,2].includes(ds.digestType) && ds.keyTag === keyId(dnskeys[j])) {
                var ksk = dnskeys[j];
                break outerloop;
            }
        }
    }

    return validateDNSKEY(Name.fromString(zone), ksk, ds);
}

async function validateDnssec(domainName, recordTypesToValidate) {
    /**
     * Verifies a zone for DNSSEC. It fetches all relevant resource records via an API, and then
     * performs verification client-side.
     * @param {string} domainName A string of the domain name to check.
     * @return {array} First element is an object with RR type keys and DNSSEC verification status
     * [see SECURE(1), INSECURE(-1), NOT_FOUND(0)]
     * Second element is a nested object of all the RRsets used in the validation process
     */
    let name = Name.fromString(domainName);
    let dnsRecords = await dnsUtils.getDnssecRRSets(name);
    const addTrailingRootLabel = true;
    let zones = name.getAllNames(addTrailingRootLabel);

    let results = {};
    recordTypesToValidate.forEach(rdtype => {results[rdtype] = NOT_FOUND;});

    for (let i = 0; i < zones.length; i++) {
        let zone = zones[i];
        if (i === 0) {
            for (let type in results) {
                try {
                    if (!(type in dnsRecords[zone])) {
                        continue;
                    }
                    zone = validateRRset(dnsRecords, zone, type);
                    results[type] = SECURE;
                } catch (e) {
                    console.error(e);
                    results[type] = INSECURE;
                }
            }
        }
        validateRRset(dnsRecords, zone, 'DNSKEY');
        if (zone !== '.') {
            // Validate the KSK DNSKEY with the DS
            validateZoneKSK(dnsRecords, zone);
            // Validate the DS RRSIG with the key in the parent zone
            validateRRset(dnsRecords, zone, 'DS');
        }
    }
    return [results, dnsRecords];
}

module.exports = {
    validateDnssec
};
